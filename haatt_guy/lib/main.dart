import 'package:flutter/material.dart';
import 'package:haatt_guy/screens/profile_screen.dart';
import 'screens/home_screen.dart';
import 'screens/signin_screen.dart';
import 'screens/splash_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/',
      routes: {
        '/': (context) => SplashScreen(),
        '/1':(context) => SignIn(),
        '/2': (context) => HomeScreen(),
        '/3': (context) => ProfileScreen()
      },
    );
  }
}
