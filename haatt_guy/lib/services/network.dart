import 'dart:developer';

import 'package:haatt_guy/utils/storage.dart';
import 'package:http/http.dart';
import 'dart:convert';

class NetworkHelper {

  Future loginData(String phone, String password) async
  {
      return post(
        Uri.parse('http://localhost:8080/haat/login'),
        headers: <String, String>{
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: jsonEncode(<String, String>{
          'phone': phone,
          'password': password
        }),
      );
  }

  Future getAnimal(String animal) async
  {

    Storage storage = Storage();
    var token = await storage.getData();

    return post(
      Uri.parse('http://localhost:8080/haat/animals/$animal'),
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: jsonEncode(<String, String>{
        'name': token.toString()
      }),
    );
  }

  Future getUser() async
  {

    Storage storage = Storage();
    var token = await storage.getData();

    return post(
      Uri.parse('http://localhost:8080/haat/user'),
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: jsonEncode(<String, String>{
        'name': token.toString()
      }),
    );
  }

}