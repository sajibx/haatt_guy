import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Storage{

  Future<dynamic> getData() async {

  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = (prefs.getString('Token') ?? "Null") ;
  log("sajib in : "+token);
  return token;
//await prefs.setString('Token', token);
}

  void setData(String token) async
  {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('Token', token);
  }

  void removeData() async
  {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('Token', "Null");
  }

}