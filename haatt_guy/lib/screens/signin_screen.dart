import 'dart:convert';
import 'dart:developer';

import 'package:alert/alert.dart';
import 'package:flutter/material.dart';
import 'package:haatt_guy/services/network.dart';
import 'package:haatt_guy/utils/storage.dart';

class SignIn extends StatefulWidget {


  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {

  String phone;
  String password;

  void testData(String phone, String password) async
  {
    NetworkHelper networkHelper = NetworkHelper();
    Storage storage = Storage();
    log("here");
    var data = await networkHelper.loginData(phone, password);
    var data1 = json.decode(data.body);
    var token = data1['name'].toString();
    log("token : "+token);
    if(token.toString().length>20)
      {
        storage.setData(token);
        Navigator.pushNamedAndRemoveUntil(context, "/2", (Route<dynamic> route) => false);
      }
    else
      {
        Alert(message: token.toString()).show();
      }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          children: [
            SizedBox(height: 20.0,),
            Row(
              children: [
                SizedBox(width: 30.0,),
                Image(image: AssetImage('images/haatt_guy.png'), width: 100.0,height: 75.0,),
              ],
            ),
            SizedBox(height: 10.0,),
            Row(
              children: [
                SizedBox(width: 30.0,),
                Text("Sign in", style: TextStyle( fontWeight: FontWeight.bold, fontSize: 25.0),)
              ],
            ),
            SizedBox(height: 10.0,),
            Row(
              children: [
                SizedBox(width: 30.0,),
                Expanded(child: Text("Welcome to The Haatt Guy. If you have already signed up, please sign in", style: TextStyle( fontSize: 15.0),))
              ],
            ),
            SizedBox(height: 30.0,),

            Padding(
              padding: const EdgeInsets.fromLTRB(35.0, 10.0, 35.0, 10.0),
              child: TextField(
                onChanged: (value) {
                  phone = value;
                },
                decoration: InputDecoration(
                  hintText: "Phone",
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black, width:  0.5),
                    borderRadius: BorderRadius.circular(20.0)
                  ),
                  border: new OutlineInputBorder(
                      borderSide: new BorderSide(color: Colors.blueGrey, width: 0.5),
                      borderRadius: BorderRadius.circular(20.0)
                  ),
            ),
              ),
            ),

            Padding(
              padding: const EdgeInsets.fromLTRB(35.0, 10.0, 35.0, 10.0),
              child: TextField(
                onChanged: (value) {
                  password = value;
                },
                obscureText: true,
                decoration: InputDecoration(
                  hintText: "Password",
                  focusedBorder:OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.blueGrey, width: 0.5),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  border: new OutlineInputBorder(
                      borderSide: new BorderSide(color: Colors.blueGrey, width: 0.5),
                      borderRadius: BorderRadius.circular(20.0)
                  ),
                ),
              ),
            ),

            SizedBox(height: 10.0,),

            Text("Forgot password?", style: TextStyle(fontWeight: FontWeight.bold),),

            SizedBox(height: 10.0,),
            
            RawMaterialButton(
              fillColor: Colors.black,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(40)),
                child: Text("Sign in", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
              padding: EdgeInsets.fromLTRB(60.0, 20.0, 60.0, 20.0),
              onPressed: () {
                testData(phone, password);
              },
            ),

            SizedBox(height: 10.0,),

            Text("Sign up", style: TextStyle(fontWeight: FontWeight.bold),)
          ],
        ),
      ),
    );
  }
}
