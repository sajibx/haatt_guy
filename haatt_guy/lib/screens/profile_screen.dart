import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:haatt_guy/services/network.dart';
import 'package:haatt_guy/utils/storage.dart';


class ProfileScreen extends StatefulWidget {


  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {

  String name;
  String email;
  String phone;
  String address;

  void getUser() async
  {
    NetworkHelper networkHelper = NetworkHelper();
    var data = await networkHelper.getUser();

    var user = json.decode(data.body);
    //log("sajib size before : "+entry1.length.toString());

    setState(() {
      name = user['name'];
      phone = user['phone'];
      email = user['email'];
      address = user['address'];
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUser();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.blueGrey[200],
        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 1.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  // border: Border.all(
                  //   color: Colors.black,
                  // ),
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(10.0),
                      bottomRight: Radius.circular(0.0),
                      topLeft: Radius.circular(10.0),
                      bottomLeft: Radius.circular(0.0)),
                ),
                  height: 50.0,
                  width: 100.0,
                  child: Center(child: Text(name??"")),
                ),
              Container(
                margin: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 1.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  // border: Border.all(
                  //   color: Colors.black,
                  // ),
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(0.0),
                      bottomRight: Radius.circular(0.0),
                      topLeft: Radius.circular(0.0),
                      bottomLeft: Radius.circular(0.0)),
                ),
                height: 50.0,
                width: 100.0,
                child: Center(child: Text(phone??"")),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 1.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  // border: Border.all(
                  //   color: Colors.black,
                  // ),
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(0.0),
                      bottomRight: Radius.circular(0.0),
                      topLeft: Radius.circular(0.0),
                      bottomLeft: Radius.circular(0.0)),
                ),
                height: 50.0,
                width: 100.0,
                child: Center(child: Text(email??"")),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 5.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  // border: Border.all(
                  //   color: Colors.black,
                  // ),
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(0.0),
                      bottomRight: Radius.circular(10.0),
                      topLeft: Radius.circular(0.0),
                      bottomLeft: Radius.circular(10.0)),
                ),
                height: 50.0,
                width: 100.0,
                child: Center(child: Text(address??"")),
              ),
              SizedBox(height: 40.0,),

              Container(
                margin: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 5.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  // border: Border.all(
                  //   color: Colors.black,
                  // ),
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(10.0),
                      bottomRight: Radius.circular(10.0),
                      topLeft: Radius.circular(10.0),
                      bottomLeft: Radius.circular(10.0)),
                ),
                height: 50.0,
                width: 100.0,
                child: Center(child: GestureDetector(child: Text("Logout", style: TextStyle(fontWeight: FontWeight.bold),), onTap: () {

                  Storage storage = Storage();
                  storage.removeData();
                  Navigator.pushNamedAndRemoveUntil(context, "/", (Route<dynamic> route) => false);

                },)),
              )
            ],
          ),
        ),
      ),
    );
  }
}
