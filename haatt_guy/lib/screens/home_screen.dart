import 'dart:convert';
import 'dart:developer';

import 'package:alert/alert.dart';
import 'package:flutter/material.dart';
import 'package:haatt_guy/services/network.dart';

class HomeScreen extends StatefulWidget {


  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<dynamic> entries = [] ;//= <String>['A', 'B', 'C'];
  String search = "goru";
  //final List<int> colorCodes = <int>[600, 500, 100];

  Future<List> getAnimal(String name) async
  {
    NetworkHelper networkHelper = NetworkHelper();
    //log("sajib search : "+search.toString()+"----"+name.toString());
    var entry = await networkHelper.getAnimal(name??"goru");
    var entry1 = json.decode(entry.body);
    //log("sajib size before : "+entry1.length.toString());
    entries.clear();
    entry1.asMap().forEach((index, value) =>
        entries.add(value)
    );
    log("sajib size after : "+entries.length.toString());
    //log("sajib : "+entries[0]['animal_name']);
    setState(() {
      entries.clear();
      entry1.asMap().forEach((index, value) =>
          entries.add(value)
      );
    });
    return entry1;
  }



  @override
  void initState() {
    super.initState();
    getAnimal(search);
    log("sajib init down : "+entries.length.toString());
    //entries = getAnimal();
    // setState(() {
    //   getAnimal(search);
    // });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          children: [
            SizedBox(height: 60.0,),
            Row(
              children: [
                SizedBox(width: 30.0,),
                Image(image: AssetImage('images/haatt_guy.png'), width: 100.0,),
                SizedBox(width: 140.0,),
                Image(image: AssetImage('images/search.png'), width: 40.0,),
                SizedBox(width: 20.0,),
                GestureDetector(child: Image(image: AssetImage('images/user.png'), width: 25.0,), onTap: () { Navigator.pushNamed(context, '/3'); },)
              ],
            ),
            SizedBox(height: 30.0,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: Image.asset('images/goat.png', width: 65.0, height: 75.0,),
                  ),
                  onTap: () { setState(() {
                    search = "sagol";
                    getAnimal(search);
                  }); },
                ),
                SizedBox(width: 40.0,),
                GestureDetector(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: Image.asset('images/cow.png', width: 65.0, height: 75.0,),
                  ),
                  onTap: () { setState(() {
                    search = "goru";
                    getAnimal(search);
                  }); },
                ),
                SizedBox(width: 40.0, height: 0,),
                GestureDetector(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: Image.asset('images/buffalo.png', width: 65.0, height: 75.0,),
                  ),
                  onTap: () { setState(() {
                    search = "buffalo";
                    getAnimal(search);
                  }); },
                )
              ],
            ),

        SizedBox(height: 10.0,),

        Expanded(
          child: ListView.builder(
              padding: const EdgeInsets.all(20),
              itemCount: entries.length,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () { Alert( message: " ${entries[index]['animal_name']} ").show(); },
                  child: Container(
                    height: 100,
                    margin: EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 10.0),
                    //color: Colors.amber[colorCodes[index]],
                    child: Container(
                        margin: EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 10.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        SizedBox(width: 5.0,),
                        Image(image: AssetImage('images/cow.png'), width: 150.0, height: 100.0,),
                        SizedBox(width: 30.0,),
                        Column(
                          children: [
                            SizedBox(height: 15.0,),
                            Text("${entries[index]['animal_name']}",style: TextStyle( fontSize: 10.0),),
                            Text("${entries[index]['animal_location']}",style: TextStyle( fontSize: 10.0),),
                            Text("${entries[index]['animal_rating']}",style: TextStyle( fontSize: 10.0),),
                            Text("${entries[index]['animal_price_new']}",style: TextStyle( fontSize: 10.0),)
                          ],
                        )
                      ],
                    )),
                  ),
                );
              }
          ),
        )

          ],
        ),
      ),
    );
  }
}
