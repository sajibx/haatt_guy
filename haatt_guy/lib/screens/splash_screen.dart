
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:haatt_guy/utils/storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {


  @override
  _SplashScreeenState createState() => _SplashScreeenState();
}

class _SplashScreeenState extends State<SplashScreen> {

  String tokens;
  void checkStatus() async
  {
    Storage storage = Storage();
    var token = await storage.getData();

    log("sajib out : "+token);
    Duration thr = Duration(seconds: 3);

    await Future.delayed(thr, () { // no other wait
      if(token=="Null")
      {
        //Navigator.pushNamed(context, '/1');
        Navigator.pushNamedAndRemoveUntil(context, "/1", (Route<dynamic> route) => false);
      }
      else
        {
          //Navigator.pushNamed(context, '/2');
          Navigator.pushNamedAndRemoveUntil(context, "/2", (Route<dynamic> route) => false);
        }
    });

    tokens = token;
    log("sajib out : "+token);
    //return token;
    //await prefs.setString('Token', token);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    checkStatus();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.white,
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircleAvatar(
                radius: 100.0,
                backgroundImage: AssetImage('images/logo.png'),

              )
            ],
          ),
        ),
      ),
    );
  }
}




